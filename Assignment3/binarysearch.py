# /bin/python
# Omkar Nath Singh
# Binary Search on Unsorted List of Characters with XML data

import xml.etree.ElementTree as ET


def merge_sort(unsorted_list):
    if len(unsorted_list) > 1:
        mid = len(unsorted_list) // 2
        left_part = unsorted_list[:mid]
        right_part = unsorted_list[mid:]

        merge_sort(left_part)
        merge_sort(right_part)

        i, j, k = 0, 0, 0
        while (i < len(left_part)) & (j < len(right_part)):
            if left_part[i] < right_part[j]:
                unsorted_list[k] = left_part[i]
                i, k = i + 1, k + 1
            else:
                unsorted_list[k] = right_part[j]
                j, k = j + 1, k + 1
        while i < len(left_part):
            unsorted_list[k] = left_part[i]
            i, k = i + 1, k + 1
        while j < len(right_part):
            unsorted_list[k] = right_part[j]
            j, k = j + 1, k + 1


def binary_search(lower, higher):
    if higher >= lower:
        mid = lower + (higher - lower) // 2

        if array[mid] == find:
            return mid
        elif find < array[mid]:
            return binary_search(lower, mid - 1)
        elif find > array[mid]:
            return binary_search(mid + 1, higher)
    else:
        return (-1)


# -------Main Logic----------------

array = []
root = ET.parse('binary_search.xml').getroot()
for i in range(len(root)):
    array.append(str(root[i].text))

merge_sort(array)
print("Sorted List: ", array)
find = str(input("Enter value to search in list: "))
position = binary_search(0, len(array) - 1)
if position == -1:
    print("Not Found")
else:
    print("Found at position: ", position)
