# Server


import hashlib
import socket


sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server_address = ('127.0.0.1', 10005)
sock.bind(server_address)

sock.listen(1)

print('Waiting for a connection...')
connection, client_address = sock.accept()

try:
    print('Connection from', client_address)

    recvData = (connection.recv(1024)).decode()
    recvHash = (connection.recv(1024)).decode()

    print('Received Message :: ' + str(recvData))
    print('Received Digest :: ' + str(recvHash))

finally:
    connection.close()

    calDigest = hashlib.sha1(str.encode(recvData)).hexdigest()
    print('Calculated Digest :: ' + str(calDigest))

    if calDigest == recvHash:
        print('Message Unaltered')
    else:
        print('Message Altered')
