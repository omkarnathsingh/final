# Client

import hashlib
import socket

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server_address = ('127.0.0.1', 10005)
sock.connect(server_address)

try:
    message = input("Message: ")
    sock.send(str.encode(message))

    digest = hashlib.sha1(str.encode(message)).hexdigest()

    print('Digest :: ' + digest)
    sock.send(str.encode(digest))

finally:
    print('Closing Socket..')
    sock.close()
