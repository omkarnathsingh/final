package com.omkar.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button button[] = new Button[20];
    float value1, value2;

    EditText editText;
    boolean add, sub, mul, div, sin, cos, tan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //numbers
        button[0] = findViewById(R.id.button10);
        button[1] = findViewById(R.id.button);
        button[2] = findViewById(R.id.button2);
        button[3] = findViewById(R.id.button3);
        button[4] = findViewById(R.id.button4);
        button[5] = findViewById(R.id.button5);
        button[6] = findViewById(R.id.button6);
        button[7] = findViewById(R.id.button7);
        button[8] = findViewById(R.id.button8);
        button[9] = findViewById(R.id.button9);
        //dot
        button[10] = findViewById(R.id.button11);
        //equal
        button[11] = findViewById(R.id.button12);
        //clear
        button[12] = findViewById(R.id.button13);
        //normal
        button[13] = findViewById(R.id.button15);
        button[14] = findViewById(R.id.button16);
        button[15] = findViewById(R.id.button17);
        button[19] = findViewById(R.id.button21);

        //tri
        button[16] = findViewById(R.id.button18);
        button[17] = findViewById(R.id.button19);
        button[18] = findViewById(R.id.button20);

        editText = findViewById(R.id.textView);

        try {
            button[0].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editText.setText(editText.getText() + "0");
                }
            });

            button[1].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editText.setText(editText.getText() + "1");
                }
            });
            button[2].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editText.setText(editText.getText() + "2");
                }
            });
            button[3].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editText.setText(editText.getText() + "3");
                }
            });
            button[4].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editText.setText(editText.getText() + "4");
                }
            });
            button[5].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editText.setText(editText.getText() + "5");
                }
            });
            button[6].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editText.setText(editText.getText() + "6");
                }
            });
            button[7].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editText.setText(editText.getText() + "7");
                }
            });
            button[8].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editText.setText(editText.getText() + "8");
                }
            });
            button[9].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editText.setText(editText.getText() + "9");
                }
            });

            button[10].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editText.setText(editText.getText() + ".");
                }
            });
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "An error occurred", Toast.LENGTH_SHORT).show();
        }

        button[12].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText(" ");
            }
        });

        try {
            button[13].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    value1 = Float.parseFloat(editText.getText() + "");
                    add = true;
                    editText.setText(null);

                }
            });

            button[14].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    value1 = Float.parseFloat(editText.getText() + "");
                    sub = true;
                    editText.setText(null);

                }
            });
            button[15].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    value1 = Float.parseFloat(editText.getText() + "");
                    mul = true;
                    editText.setText(null);

                }
            });

            button[19].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    value1 = Float.parseFloat(editText.getText() + "");
                    div = true;
                    editText.setText(null);

                }
            });
        } catch (NumberFormatException e) {
            Toast.makeText(getApplicationContext(), "Enter number first", Toast.LENGTH_SHORT).show();
        }

        button[16].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText("Sin");
                sin = true;
            }
        });
        button[17].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText("Cos");
                cos = true;
            }
        });
        button[18].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText("Tan");
                tan = true;
            }
        });


        button[11].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (sin) {
                        String Text = editText.getText().toString().replace("Sin", " ");
                        Double Answer = Math.sin(Math.toRadians(Double.parseDouble(Text)));
                        editText.setText(Answer.toString());
                        sin = false;
                    }

                    if (cos) {
                        String Text = editText.getText().toString().replace("Cos", " ");
                        Double Answer = Math.sin(Math.toRadians(Double.parseDouble(Text)));
                        editText.setText(Answer.toString());
                        cos = false;
                    }

                    if (tan) {
                        String Text = editText.getText().toString().replace("Tan", " ");
                        Double Answer = Math.sin(Math.toRadians(Double.parseDouble(Text)));
                        editText.setText(Answer.toString());
                        tan = false;
                    }

                    value2 = Float.parseFloat(editText.getText() + "");

                    if (add) {
                        editText.setText(value1 + value2 + "");
                        add = false;
                    }

                    if (sub) {
                        editText.setText(value1 - value2 + "");
                        sub = false;
                    }

                    if (mul) {
                        editText.setText(value1 * value2 + "");
                        mul = false;
                    }
                    if (div) {
                        editText.setText(value1 / value2 + "");
                        div = false;
                    }
                } catch (NumberFormatException e) {
                    Toast.makeText(getApplicationContext(), "Invalid Operation", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }
}
