class OES():
    def __init__(self, arr):
        self.res = self.sort(arr)

    def sort(self, arr):
        isSorted = False
        while (not isSorted):
            isSorted = True
            for i in range(1, len(arr)-1, 2):
                if (arr[i] > arr[i + 1]):
                    t = arr[i]
                    arr[i] = arr[i+1]
                    arr[i+1] = t
                    isSorted = False
            for i in range(0, len(arr)-1, 2):
                if (arr[i] > arr[i + 1]):
                    t = arr[i]
                    arr[i] = arr[i+1]
                    arr[i+1] = t
                    isSorted = False
        return arr