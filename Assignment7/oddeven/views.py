from django.shortcuts import render
import oddeven.Backend.oddeven

def main(req):
    if req.method == "POST":
        arr = []
        search_id = req.POST.get('textfield', None)
        arr = list(map(int, search_id.split()))
        arr = oddeven.Backend.oddeven.OES(arr).res
        return render(req, 'index.html', {"res" : arr})
    else:
        return render(req, 'index.html')