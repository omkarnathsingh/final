# /bin/python
# Concurrent Quick Sort using XML data
# Omkar Nath Singh

import xml.etree.ElementTree as ET
import threading


# ------------Function Definitions-----------------
def partition(lower, higher):
    pivot = array[higher]
    i = lower - 1
    for j in range(lower, higher - 1):
        if array[j] < pivot:
            i = i + 1
            array[i], array[j] = array[j], array[i]
    if array[higher] < array[i + 1]:
        array[i + 1], array[higher] = array[higher], array[i + 1]
    return i + 1


def quick_sort(lower, higher):
    if lower < higher:
        pivot = partition(lower, higher)

        t1 = threading.Thread(target=quick_sort, args=(lower, pivot - 1))
        t1.start()
        t2 = threading.Thread(target=quick_sort, args=(pivot + 1, higher))
        t2.start()

        t1.join()
        t2.join()


# ---------------------Main Logic------------------

root = ET.parse('quicksort.xml').getroot()
array = []
for i in range(len(root)):
    array.append(int(root[i].text))

print("Data from XML: ", array)
quick_sort(0, len(array) - 1)
print("Sorted Array: ", array)
