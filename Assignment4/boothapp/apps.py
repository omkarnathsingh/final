from django.apps import AppConfig


class BoothappConfig(AppConfig):
    name = 'boothapp'
