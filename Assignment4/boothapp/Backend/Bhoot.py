def ToBinary(x, bits):
	temp = x
	b = []
	#find the binary value
	while temp != 0:
		t = temp % 2
		temp = int(temp/2)
		b.append(t)
	b.reverse()
	#add left remaining bits
	if(len(b) <= bits):
		temp = [0]*(bits-len(b))
		temp = temp + b
	#if number is negative, perform 2's complement
		if x < 0:
			temp = [0 if i else 1 for i in temp]
			temp[-1] += 1
			#perform addition according to the convention
			for i in range(bits-1, 0, -1):
				if temp[i] == 2:
					temp[i] = 0
					temp[i-1] += 1
		return temp
	else:
		print("Bit count chukavlaay")
		exit(0)

def ToDecimal(x):
	negative = False
	#if most significant bit is 1 number is negative, perform reverse of 2's complement
	if x[0]:
		negative = True
		x[-1] -= 1
		for i in range(len(x)-1, 0, -1):
			if x[i] == -1:
				x[i] = 1
				x[i-1] -= 1
		x = [0 if i else 1 for i in x]
	ans = 0
	for i in range(len(x)):
		ans += (2**i) * x[len(x)-1-i]
	return -ans if negative else ans

def ArithmeticRightShift(data):
	for i in range(len(data)-1, 0, -1):
		data[i] = data[i-1]
	return data

def AddToA(data, m):
	c = 0
	for i in range(len(m)-1, -1, -1):
		data[i] = data[i] + m[i] + c
		if data[i] >= 2:
			data[i] -= 2
			c = 1
		else:
			c = 0
	return data

def SubFromA(data, m):
	for i in range(len(m)-1, -1, -1):
		data[i] = data[i] - m[i]
		if data[i] < 0:
			data[i] += 2
		#
			if i-1 >= 0:
				data[i-1] -= 1
	return data

def Calc(m, q, bits):
	arr = []
	data = [0]*(bits)
	m = ToBinary(m, bits)
	q = ToBinary(q, bits)
	data += q
	data.append(0)
	for _ in range(bits):
		if data[-1] == data[-2]:
			data = ArithmeticRightShift(data)
		elif data[-1] == 0:
			data = SubFromA(data,m)
			data = ArithmeticRightShift(data)
		else:
			data = AddToA(data, m)
			data = ArithmeticRightShift(data)
		x = []
		for i in data:
			x.append(i)
		arr.append(x)
	return (ToDecimal(data[:-1])),data[:-1]

