from django.http import HttpResponse
from django.shortcuts import render
from boothapp.Backend.Bhoot import Calc
# Create your views here.
def main(req):
    if req.method == 'POST':
        n1 = req.POST.get("Number1", None)
        n2 = req.POST.get("Number2", None)
        ansd, ansb = Calc(int(n1), int(n2), 8)
        arr = []
        arr.append(ansd)
        arr.append(ansb)
        return render(req, 'index.html', {'res': arr})
    else:
        return render(req, 'index.html')

