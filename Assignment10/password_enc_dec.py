#! /bin/python3


def caesar_cipher_encrypt(plain_text, shift):
    cipher_text = ""
    for ch in plain_text:
        if ch.isalpha():
            stay_in_alphabet = ord(ch) + shift
            if stay_in_alphabet > ord('z'):
                stay_in_alphabet -= 26
            final_letter = chr(stay_in_alphabet)
            cipher_text += final_letter
    return cipher_text


def caesar_cipher_decrypt(plain_text, shift):
    cipher_text = ""
    for ch in plain_text:
        if ch.isalpha():
            stay_in_alphabet = ord(ch) - shift
            if stay_in_alphabet > ord('z'):
                stay_in_alphabet -= 26
            final_letter = chr(stay_in_alphabet)
            cipher_text += final_letter
    return cipher_text


password = input('Enter your password: ')
encrypted_password = caesar_cipher_encrypt(password, 3)
print('Your encrypted password:', encrypted_password)

password_verify = input('Enter your password for verification: ')

if(caesar_cipher_decrypt(encrypted_password, 3) == password_verify):
    print("Authentication Successful!")
else:
    print("Authentication Failed!")
