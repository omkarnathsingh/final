import json
import os
import sys

N = 8
board = [[0 for j in range(N)] for x in range(N)]


def getLocation():
    with open('queen.json') as f:
        data = json.load(f)
        col = data["col"]
        if col > N - 1:
            print('Cannot place Queen at ', col)
            sys.exit(0)
        board[0][col] = 1
    return col


def isSafe(i, j):
    # checking if there is a queen in row or column
    for k in range(0, N):
        if board[i][k] == 1 or board[k][j] == 1:
            return False
    # checking diagonals
    for k in range(0, N):
        for l in range(0, N):
            if (k + l == i + j) or (k - l == i - j):
                if board[k][l] == 1:
                    return False
    return True


def solve(row):
    i = 0
    while i < N:
        if isSafe(row, i):
            board[row][i] = 1
            if row == N - 1:
                return True
            else:
                if solve(row + 1):
                    return True
                else:
                    board[row][i] = 0
        i += 1
    if i == N:
        return False


def printBoard():
    for i in board:
        print(i)


col = getLocation()
done = solve(1)
if done:
    print("\nSolution :\n")
    printBoard()
else:
    print('Cannot find solution when queen placed at position', col + 1)
