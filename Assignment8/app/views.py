from django.shortcuts import render
import app.Backend.plag
# Create your views here.

def checkplag(req):
    if req.method == "POST":
        input1 = str(req.POST.get('text1'))
        input2 = str(req.POST.get('text2'))
        target = str(req.POST.get('target'))
        plagper = app.Backend.plag.checkPlag(input1, input2, target)
        percent = plagper.percent
        return render(req,"plagerout.html",{"result":percent})
    else:
        return render(req,"plager.html")